@Echo off
rem (c) me@koi8-r.name
Title Installing Windows updates

Echo "Change dir to: %~dp0"
CD /D %~dp0

SET /A FILES=0
SET /A INSTALLED=0
For %%F In (*.msu *.exe *.cab) Do SET /A FILES=FILES+1
Echo %FILES% files to install

For %%M In (*.msu) Do CALL :MSU %%M
For %%E In (*.exe) Do CALL :EXE %%E
For %%C In (*.cab) Do CALL :CAB %%C
GOTO:EOF

:MSU
SET /A INSTALLED=INSTALLED+1
Echo "(%INSTALLED%/%FILES%) Installing %1"
Start /Wait %1 /quiet /norestart
GOTO:EOF

:EXE
SET /A INSTALLED=INSTALLED+1
Echo "(%INSTALLED%/%FILES%) Installing %1"
Start /Wait %1 /q /norestart
GOTO:EOF

:CAB
SET /A INSTALLED=INSTALLED+1
Echo "(%INSTALLED%/%FILES%) Installing %1"
Start /Wait pkgmgr /ip /m:%1 /quiet /norestart
GOTO:EOF

EXIT